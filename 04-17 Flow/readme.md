# Flow #

![picture](https://lh3.googleusercontent.com/lPQYdn_kgUWXu1dqhXE5sWTFHMhfoS8tGqCIjfYZl3j2lGtRihkBpLbB5-nBCd1MCQ=w300)

## Problem: ##
You are given a matrix of characters NxN in size. This matrix contains pairs of characters (a-z) as well as empty spaces represented by zeros (0). For each character pair, you must create a continous path of that character that connects from one intial point to the other. This can be done by replacing only empty spaces.

## Constraints: ##
The first line of input is a string containing each character in the puzzle.  
The second line is N.  
The next N lines will represent the matrix. Each line is N long.  

## Example: ##

### Input: ###

bgroy  
5  
r0g0y  
00b0o  
00000  
0g0y0  
0rbo0  


### Desired Output: ###

rggyy  
rgbyo  
rgbyo  
rgbyo  
rrboo  

## Testing ##
You can test your solution using any command line programming by running the included test.py:

`python test.py input.txt solutions.txt "python your-program.py"`

Three test cases are included but any number can be added. Any command can be given as the executable string. A process will be spawned for each input to execute that string. Input is passed as STDIN. The resulting STDOUT will be compared against the solution for the given test case.