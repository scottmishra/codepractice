import json, random

file = open('languages.json')
data = json.load(file)
langs = data['counts']
file.close()

# Make random selections
m = min(langs.items(), key=lambda x: x[1])[1]
possible = [str(x[0]) for x in langs.items() if x[1] == m]
picks = random.sample(possible,min(3, len(possible)))

# Ask for input
print("Randomly Selected: "+str(picks).strip('[]')).replace('\'','')
res = raw_input("Enter Y/y to commit or anything else to decline: ")

# Commit selected languages to languages.json
if res.lower() == 'y':
    print("Accepted: Committing changes...")
    for i in picks:
        langs[unicode(i)] += 1
    data['counts'] = langs
    data['current'] = picks
    file = open('languages.json', 'w')
    json.dump(data, file, indent=2)
    file.close()
else:
    print("Declined: Run again to try another selection.")

